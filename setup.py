from setuptools import setup

setup(
    name='Utilities',
    version='0.1.0',
    author='Maximilian Benkert',
    author_email='',
    packages=['utilities'],
    # scripts=[],
    # url='http://pypi.python.org/pypi/Utilities/',
    license='LICENSE',
    description='Some useful utilities',
    long_description=open('README.md').read(),
    install_requires=[],
)
