import os


def get_files_in_directory(path):
    """List names of files in a given directory.

    :param path: path to directory
    :return: names of files as list
    """
    return [
        file
        for file in os.listdir(path)
        if os.path.isfile(os.path.join(path, file))
    ]
